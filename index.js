const express = require('express')
const bodyParser = require('body-parser')
const request = require('superagent')
const satellite = require('./satellite.js').satellite
const SerialPort = require('serialport')
const fs = require('fs')
const shell = require('shelljs')
const _ = require('lodash')

console.log('init entangled, hold tight...')

let debug = true

const app = express()
app.use(bodyParser.urlencoded({ extended: true }))

if (!debug) {
	const port = new SerialPort('/dev/serial0', {
		baudRate: 19200
	}, function (err) {
		if (err) {
			return console.log('Error: ', err.message)
		}
		console.log('serial port ok!')
	})
}

let screen = []

/*
let digits = [
	[62,34,62], // 0
	[34,62,2],  // 1
	[46,42,58], // 2
	[42,42,62], // 3
	[56,8,62],  // 4
	[58,42,46], // 5
	[62,42,46], // 6
	[32,46,48], // 7
	[62,42,62], // 8
	[58,42,62]
]
*/



let digits = [
	[62,34,62], // 0
	[34,62,32],  // 1
	[58,42,46], // 2
	[42,42,62], // 3
	[14,8,62],  // 4
	[46,42,58], // 5
	[62,42,58], // 6
	[2,58,6], // 7
	[62,42,62], // 8
	[46,42,62] // 9
]

let tle = ''
let iss

// Berlin
var observerGd = {
	latitude: 52.5284076 * satellite.constants.deg2rad,
	longitude: 13.4072528 * satellite.constants.deg2rad,
	height: 0.034
}

function computeDistance(){

	let positionAndVelocity = satellite.propagate(iss, new Date())
	let positionEci = positionAndVelocity.position

	let gmst = satellite.gstimeFromDate(new Date())

	let positionGd = satellite.eciToGeodetic(positionEci, gmst)
	let positionEcf = satellite.eciToEcf(positionEci, gmst)
	let observerEcf = satellite.geodeticToEcf(observerGd)

	/*
	let latitude  = positionGd.latitude
	let longitude = positionGd.longitude
	let height    = positionGd.height

	let latitudeStr  = satellite.degreesLat(latitude)
	let longitudeStr = satellite.degreesLong(longitude)
	*/

	return Math.round(Math.sqrt(Math.pow(positionEcf.x - observerEcf.x, 2) + Math.pow(positionEcf.y - observerEcf.y,2) + Math.pow(positionEcf.z - observerEcf.z, 2))*1000)
}

function publishDistance(){

	/*
	// send to Particle API
	request.post('https://api.particle.io/v1/devices/events')
		.send('access_token='+process.env.PARTICLE_TOKEN)
		.send('name=entangled')
		.send('data='+computeDistance())
		.send('private=true')
		.send('ttl=5')
		.end()
	*/

	// send over serial
	display(computeDistance())
}

function display(distance){

	initScreen()
	setScreen(distance)

	screen = _.reverse(screen)

	for (let i=0; i<2; i++){
		port.write([0x80]) // start frame
		port.write([0x83]) // command id
		port.write([i]) // address
		for (let j=0; j<28; j++){
			port.write([screen[i*28+j]]) // set row
		}
		port.write([0x8F]) // close frame
	}
}

function initScreen(){
	for (let i=0; i<56; i++){
		screen[i] = 0
	}

	// display m
	screen[1] = 56
	screen[2] = 8
	screen[3] = 56
	screen[4] = 8
	screen[5] = 56
}

function setScreen(distance){
	let s = distance.toString()
	for (let i=0; i<s.length && i<8; i++){
		screen[7+i*4] = digits[getIndex(s[s.length-1-i])][2]
		screen[7+i*4+1] = digits[getIndex(s[s.length-1-i])][1]
		screen[7+i*4+2] = digits[getIndex(s[s.length-1-i])][0]
	}
}

function getIndex(c){
	switch (c) {
		case '0':
			return 0
		case '1':
			return 1
		case '2':
			return 2
		case '3':
			return 3
		case '4':
			return 4
		case '5':
			return 5
		case '6':
			return 6
		case '7':
			return 7
		case '8':
			return 8
		case '9':
			return 9
		default:
			return 0
	}
}

app.get('/', function(req,res){
	res.send('Thibault Brevet<br><i>Entangled (ISS)</i>, 2017<br><br>The ISS is currently at '+computeDistance()+'m from Berlin.')
})

app.get('/update', function(req,res){
	res.sendFile('update.html', {root: __dirname + '/'})
})

app.post('/tle', function(req,res){
	res.sendStatus(200)
	console.log('received fresh TLE')
	console.log(req.body.tle)
	fs.writeFile('/home/pi/entangled/tle.txt', req.body.tle)
})

app.get('/shutdown', function(req,res){
	res.send('shutting down...')
	shell.exec('sudo halt')
})

app.listen(process.env.PORT || 3000, function () {

	fs.readFile('/home/pi/entangled/tle.txt', 'utf8', function(err, data){

		if (!err) {
			tle = [ data.split('\n')[0], data.split('\n')[1] ]
		} else {
			console.log('duh, cannot read TLE file! continuing with hardcoded fallback TLEs, MIGHT BE STALE!')
			tle = [
				'1 25544U 98067A   18059.87764904  .00001891  00000-0  35750-4 0  9991',
				'2 25544  51.6406 198.6608 0003001 139.2650 358.3098 15.54165571101673'
			]
		}

		console.log('using TLE:')
		console.log(tle[0])
		console.log(tle[1])

		iss = satellite.twoline2satrec(tle[0], tle[1])

		if (!debug) {
			console.log('booted up, starting screen..')
			setInterval(() => {
				publishDistance()
			}, 50)
		}

	})

	console.log('Entangled backend running on port', process.env.PORT || 3000)

})
