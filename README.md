# ENTANGLED v2

- `git clone` on Raspberry Pi Zero, install Node and dependencies
- install autostart script on RasPi

This is the node script running on the Raspberry Pi to control AlfaZeta flip-dot displays for *Entangled*.

- A webpage is served on port `3000` unless specified as en env variable showing current computed target distance.
- Update of the target TLEs is possible on route `/update`.
- request `/shutdown` for clean RasPi shutdown.

### © Thibault Brevet, 2017.